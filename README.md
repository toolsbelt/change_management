## Change Management

System control to products versions

#### Tech

* python 3.6.6
* Flask-RESTful
* Flask-SQLAlchemy
* Mysql

#### Information

> The main file is `app.py`

> Using gunicorn `gunicorn app:app`

#### Using mysql with Docker

https://hub.docker.com/_/mysql/

> docker pull mysql

```bash
# database -> Integration test and System Test
$ docker run --name change_management_test -p 3306:3306 -e MYSQL_DATABASE=change_management_test -e MYSQL_USER=sa -e MYSQL_PASSWORD=sa -e MYSQL_ALLOW_EMPTY_PASSWORD=no -e MYSQL_ROOT_PASSWORD=sa -d mysql
# database -> Application
$ $ docker run --name change_management -p 3306:3306 -e MYSQL_DATABASE=change_management -e MYSQL_USER=sa -e MYSQL_PASSWORD=sa -e MYSQL_ALLOW_EMPTY_PASSWORD=no -e MYSQL_ROOT_PASSWORD=sa -d mysql
```