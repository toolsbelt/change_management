from models.products import Products
from test.test_base import TestBase


class TestProducts(TestBase):
    PRODUCT_CONTRACT = Products(
        name="Test name",
        description="Product description",
        repository_url="https://teste.com.br",
        workspace_documentation="https://teste.com.br")

    def test_create_product(self):
        with self.app_context():
            self.PRODUCT_CONTRACT.create_product()

            result = Products.find_by_slug(self.PRODUCT_CONTRACT.slug)

            self.assertIsNotNone(result)
            self.assertEqual(self.PRODUCT_CONTRACT.name, result.name)

    def test_create_product_without_required_fields(self):
        pass

    def test_create_duplicate_products(self):
        pass

    def test_find_get_all_products(self):
        pass
