from app import app
from unittest import TestCase
from configuration.database_configuration import db
from models.products import Products
from models.changelog import Changelog
from models.features import Features


class TestBase(TestCase):

    def setUp(self):
        app.config["SQLALCHEMY_DATABASE_URI"] = "mysql+pymysql://sa:sa@localhost:3306/change_management_test"
        with app.app_context():
            db.init_app(app)
            db.create_all()

        self.app = app.test_client()
        self.app_context = app.app_context

    def tearDown(self):
        with self.app_context():
            db.session.remove()
            db.drop_all()
