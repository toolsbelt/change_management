from models.products import Products
from test.test_base import TestBase


class TestProducts(TestBase):

    PRODUCT_CONTRACT = {
        "name": "Test name",
        "description": "Product description",
        "repository_url": "https://teste.com.br",
        "workspace_documentation": "https://teste.com.br"
    }

    PRODUCT_CONTRACT_1 = {
        "name": "Test name 1",
        "description": "Product 1 description",
        "repository_url": "https://teste1.com.br",
        "workspace_documentation": "https://teste1.com.br"
    }

    PRODUCT_CONTRACT_WITHOUT_NAME = {
        "description": "Product description",
        "repository_url": "https://teste.com.br",
        "workspace_documentation": "https://teste.com.br"
    }

    def test_create_product(self):
        with self.app_context():
            result = self.app.post("/products", data=self.PRODUCT_CONTRACT)

            self.assertIsNotNone(result)

            self.assertEqual(201, result.status_code)
            self.assertTrue("uri" in result.json)

    def test_create_product_without_required_fields(self):
        with self.app_context():
            result = self.app.post("/products", data=self.PRODUCT_CONTRACT_WITHOUT_NAME)

            self.assertIsNotNone(result)

            self.assertEqual(400, result.status_code)
            self.assertFalse("uri" in result.json)

    def test_create_duplicate_products(self):
        with self.app_context():
            result = self.app.post("/products", data=self.PRODUCT_CONTRACT)
            result = self.app.post("/products", data=self.PRODUCT_CONTRACT)

            self.assertIsNotNone(result)

            self.assertEqual(409, result.status_code)
            self.assertFalse("uri" in result.json)

    def test_find_get_all_products(self):
        with self.app_context():
            result = self.app.post("/products", data=self.PRODUCT_CONTRACT)
            result = self.app.post("/products", data=self.PRODUCT_CONTRACT_1)

            result = self.app.get("/products")

            self.assertIsNotNone(result)

            self.assertEqual(200, result.status_code)
            self.assertFalse("uri" in result.json)
            self.assertTrue("products" in result.json)
            self.assertTrue(2 == len(result.json["products"]))
