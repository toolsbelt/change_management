import logging
import sys

SYSTEM_OUT = logging.StreamHandler(sys.stdout)

SYSTEM_OUT.setLevel(logging.INFO)

FORMATTER = logging.Formatter('[%(asctime)s] %(levelname)s in %(module)s: %(message)s')

SYSTEM_OUT.setFormatter(FORMATTER)

logger = logging.getLogger("change_management")

logger.setLevel(logging.INFO)

logger.addHandler(SYSTEM_OUT)
