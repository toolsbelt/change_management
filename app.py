from uuid import uuid4
from resources.products_resource import ProductsResource
from flask_restful import Api
from configuration.database_configuration import db
from flask import Flask

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "mysql+pymysql://sa:sa@localhost:3306/change_management"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True
app.config["PROPAGATE_EXCEPTIONS"] = True

app.secret_key = str(uuid4())
db.init_app(app)

api = Api(app)

api.add_resource(ProductsResource, "/products", endpoint="products_resource")

if __name__ == '__main__':
    app.run(port=5000)
