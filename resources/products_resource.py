from flask import url_for
from flask_restful import Resource
from sqlalchemy.exc import IntegrityError
from werkzeug.exceptions import BadRequest

from parser.product_parse import parse_args
from models.products import Products
from configuration.logging_configuration import logger


class ProductsResource(Resource):

    def post(self):
        try:
            data = parse_args()
            product = Products(**data)
            product.create_product()

            logger.info("Product create with success")
            return {"uri": url_for("products_resource", slug=product.slug)}, 201
        except BadRequest as e:
            logger.error("Error during create product", e)
            return {"message": "Error during create product"}, e.code
        except IntegrityError as e:
            logger.error("Error during create product", e)
            return {"message": "Error during create product"}, 409
        except Exception as e:
            logger.error("Error during create product", e)
            return {"message": "Error during create product"}, 500

    def get(self):
        try:
            products = [item.json() for item in Products.get_all_products()]
            logger.info("get all products size {}".format(len(products)))

            return {"products": products}, 200
        except Exception as e:
            logger.error("Error during get all product", e)
            return {"message": "Error during get all product"}, 500
