from datetime import datetime
from slugify import slugify
from configuration.database_configuration import db


class Products(db.Model):

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(100), nullable=False)
    registration_date = db.Column(db.DateTime, nullable=False)
    slug = db.Column(db.String(80), nullable=False, unique=True)
    repository_url = db.Column(db.String(200), nullable=False, unique=True)
    workspace_documentation = db.Column(db.String(200), nullable=False, unique=True)
    description = db.Column(db.String(200), nullable=False)

    changelog = db.relationship("Changelog", backref="products")

    def __init__(self, name, description, repository_url, workspace_documentation):
        self.name = name
        self.registration_date = datetime.now()
        self.description = description
        self.repository_url = repository_url,
        self.workspace_documentation = workspace_documentation
        self.slug = slugify(name)

    def create_product(self):
        db.session.add(self)
        db.session.commit()

    def delete_product(self):
        db.session.remove(self)
        db.session.commit()

    @classmethod
    def find_by_slug(cls, slug):
        return cls.query.filter_by(slug=slug).first()

    @classmethod
    def get_all_products(cls):
        return cls.query.all()

    def json(self):
        return {
            "id": self.id,
            "name": self.name,
            "registration_date": self.registration_date.isoformat(),
            "repository_url": self.repository_url,
            "workspace_documentation": self.workspace_documentation,
            "description": self.description,
            "slug": self.slug,
            "changelog": [item.pocket_json() for item in self.changelog]
        }

    def pocket_json(self):
        return {
            "id": self.id,
            "name": self.name,
            "registration_date": self.registration_date.isoformat(),
            "repository_url": self.repository_url,
            "workspace_documentation": self.workspace_documentation,
            "description": self.description,
            "slug": self.slug
        }
