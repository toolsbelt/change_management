from datetime import datetime

from configuration.database_configuration import db


class Changelog(db.Model):

    id = db.Column(db.BigInteger, unique=True, primary_key=True, autoincrement=True)
    registration_date = db.Column(db.Date, nullable=False)
    branch = db.Column(db.String(100))
    last_commit = db.Column(db.String(200))
    tag = db.Column(db.String(20))
    status = db.Column(db.Boolean)
    artifact_url = db.Column(db.String(200))

    product_id = db.Column(db.Integer, db.ForeignKey("products.id"), nullable=False)
    product = db.relationship("Products")

    features = db.relationship("Features")

    def __init__(self, registration_date, branch, last_commit, tag, artifact_url, product_id):
        self.registration_date = registration_date
        self.branch = branch
        self.last_commit = last_commit
        self.registration_date = datetime.now()
        self.tag = tag
        self.status = "open"
        self.artifact_url = artifact_url

        self.product_id = product_id

    def create_changelog(self):
        db.session.add(self)
        db.session.commit()

    def delete_changelog(self):
        db.session.remove(self)
        db.session.commit()

    @classmethod
    def get_changelog_by_product_id(cls, product_id):
        return cls.query.filter_by(product_id=product_id)

    def json(self):
        return {
            "id": self.id,
            "product": self.product.pocket_json(),
            "branch": self.branch,
            "last_commit": self.last_commit,
            "registration_date": self.registration_date,
            "tag": self.tag,
            "status": self.status,
            "artifact_url": self.artifact_url,
            "features": [item.pocket_json() for item in self.features]
        }

    def pocket_json(self):
        return {
            "id": self.id,
            "branch": self.branch,
            "last_commit": self.last_commit,
            "registration_date": self.registration_date,
            "tag": self.tag,
            "status": self.status,
            "artifact_url": self.artifact_url
        }
