from configuration.database_configuration import db


class Features(db.Model):
    id = db.Column(db.BigInteger, primary_key=True, autoincrement=True)
    name = db.Column(db.String(100), nullable=False)
    description = db.Column(db.String(400), nullable=False)
    url = db.Column(db.String(200))

    changelog_id = db.Column(db.BigInteger, db.ForeignKey("changelog.id"), nullable=False)
    changelog = db.relationship("Changelog")

    def __init__(self, name, description, url, changelog_id):
        self.name = name
        self.description = description
        self.url = url
        self.changelog_id = changelog_id

    def create_feature(self):
        db.session.add(self)
        db.session.commit()

    def delete_feature(self):
        db.session.remove(self)
        db.session.commit()

    @classmethod
    def get_all_features_by_changelog_id(cls, changelog_id):
        return cls.query.filter_by(changelog_id)

    def json(self):
        return {
            "id": self.id,
            "name": self.name,
            "description": self.description,
            "url": self.url,
            "changelog": self.changelog.pocket_json()
        }

    def pocket_json(self):
        return {
            "id": self.id,
            "name": self.name,
            "description": self.description,
            "url": self.url
        }
