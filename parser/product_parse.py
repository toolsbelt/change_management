from flask_restful import reqparse
from datetime import date, datetime


def parse_args():
    parse = reqparse.RequestParser()
    parse.add_argument("name", type=str, required=True, help="Name is required")
    parse.add_argument("description", type=str, required=True, help="Description is required")
    parse.add_argument("repository_url", type=str, required=True, help="Repository is required")
    parse.add_argument("workspace_documentation", type=str, required=True, help="Workspace is required")

    return parse.parse_args()


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError ("Type %s not serializable" % type(obj))
